package jp.example.freshmandemo;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyStoreAdapter extends ArrayAdapter<MyStoreData>{
	private LayoutInflater mInflater;

	public MyStoreAdapter(Context context , int textViewResourceId, List<MyStoreData> objects){
		super(context,textViewResourceId,objects);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		MyStoreData StoreData = getItem(position);
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.list_store_item, null);

		}

		TextView text1 = (TextView) convertView.findViewById(R.id.Text1);
		text1.setText(StoreData.Name);
		TextView text3= (TextView) convertView.findViewById(R.id.Text3);
		text3.setText(StoreData.LimitDate);
		TextView text2= (TextView) convertView.findViewById(R.id.Text2);
		text2.setText(StoreData.Num);
		return convertView;
	}


}
